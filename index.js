const path = require('path')
const port = 4412

// express
const express = require('express')
const app = express()
const bodyParser = require('body-parser')
app.use(bodyParser.json())

// request logging
const intel = require('intel')
const formatter = new intel.Formatter({ format: '[%(date)s] %(message)s:' })
const consoleHandler = new intel.handlers.Console({
    formatter: formatter,
})
const fileHandler = new intel.handlers.File({
    file: path.join(__dirname, 'request_logs', 'request.log'),
    formatter: formatter,
})
intel.addHandler(fileHandler)

function composeLogMessage(req) {
    return {
        from: req.ip,
        to: req.originalUrl,
        body: req.body,
    }
}

app.post('/nodle/pub', (req, res) => {
    intel.info(composeLogMessage(req))
    res.setHeader('Content-Type', 'application/json')
    res.end(JSON.stringify({
        data: 'Succesfully connected to /nodle/pub'
    }))
})

app.all('*', (req, res) => {
    intel.info(composeLogMessage(req))
    res.setHeader('Content-Type', 'application/json')
    res.statusCode = 404
    res.end(JSON.stringify({
        data: 'Please send requests to /nodle/pub instead'
    }))
})

app.listen(port)
